package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class RegistryPage {

    WebDriver driver;
    WebDriverWait driverWait;
    String getTextResult, getDateResult;
    RegistryModel registryModel = new RegistryModel();
    RegistryLocator registryLocator = new RegistryLocator();

    public RegistryPage(WebDriver driver) {
        this.driver = driver;
        driverWait = new WebDriverWait(driver, 15);
    }

    public void findRegistrantName(String registrant, String type) {
        driver.findElement(registryLocator.closeDialogLocation).click();
        driver.findElement(registryLocator.tabRegistry).click();
        driverWait.until(ExpectedConditions.visibilityOfElementLocated(registryLocator.titleLabelRegistry));
        String getTitleRegistry = driver.findElement(registryLocator.titleLabelRegistry).getText();
        if (!getTitleRegistry.isEmpty()) System.out.println("Title label found: " + getTitleRegistry);
        driver.findElement(registryLocator.inputRegistrantName).sendKeys(registrant);
        selectOptionByValue(registryLocator.selectRegistryType, type);
        driver.findElement(registryLocator.buttonSearchRegistry).click();
        System.out.println("Search registrant " + registrant + " with type " + type);
        driverWait.until(ExpectedConditions.visibilityOfElementLocated(registryLocator.filterDateFrom));
        driverWait.until(ExpectedConditions.elementToBeClickable(registryLocator.filterDateFrom));
        selectOptionByValue(registryLocator.filterDateFrom, registryModel.selectFilterJan);
        selectOptionByValue(registryLocator.filterDateTo, registryModel.selectFilterApr);
        driver.findElement(registryLocator.filterYearFrom).click();
        driver.findElement(registryLocator.chooseYearFrom).click();
        driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
        driver.findElement(registryLocator.filterYearTo).click();
        driver.findElement(registryLocator.chooseYearTo).click();
        driverWait.until(ExpectedConditions.visibilityOfElementLocated(registryLocator.buttonApplyFilter));
        driverWait.until(ExpectedConditions.elementToBeClickable(registryLocator.buttonApplyFilter));
        driver.findElement(registryLocator.buttonApplyFilter).click();
    }

    public void verifyRegistrantAndDate() {
        driverWait.until(ExpectedConditions.visibilityOfElementLocated(registryLocator.resultRegistrant));

        List<WebElement> names = driver.findElements(registryLocator.resultRegistrant);
        List<WebElement> dates = driver.findElements(registryLocator.resultDates);
        dates.remove(0);
        if (names.size() > 1 && dates.size() > 1) {
            getTextResult = driver.findElement(registryLocator.resultRegistrant).getText();
            getDateResult = driver.findElement(registryLocator.resultDates).getText();
            if (getTextResult.contains(registryModel.registrantName) && getDateResult.contains("2021")) {
                System.out.println("Name " + getTextResult + " found with total data: " + names.size());
                System.out.println(getDateResult + " text corrected with total data: " + dates.size());
            }
        }
        Assert.assertEquals(names.size() > 1, getTextResult.contains(registryModel.registrantName));
    }

    private void selectOptionByValue(By locator, String value) {
        Select select = new Select(driver.findElement(locator));
        select.selectByValue(value);
    }
}