package pages;

import org.openqa.selenium.By;

public class RegistryLocator {

    public By closeDialogLocation = By.xpath("//input[@data-action-type='DISMISS']");
    public By tabRegistry = By.xpath("//a[text()='Registry']");
    public By titleLabelRegistry = By.xpath("//div[@class='gr-header gr-header--lg']");
    public By inputRegistrantName = By.xpath("//input[@placeholder='Search by Registrant name']");
    public By selectRegistryType = By.xpath("//select[@name='searchUrl']");
    public By buttonSearchRegistry = By.xpath("//button[@aria-label='Search']");
    public By filterDateFrom = By.id("gr-search-from-month-dropdown");
    public By filterDateTo = By.id("gr-search-to-month-dropdown");
    public By buttonApplyFilter = By.xpath("//input[@aria-labelledby='a-autoid-5-announce']");
    public By filterYearFrom = By.xpath("//span[@id='a-autoid-2-announce']");
    public By filterYearTo = By.xpath("//span[@id='a-autoid-4-announce']");
    public By chooseYearFrom = By.xpath("(//*[@data-value='{\"stringVal\":\"2021\"}'])[1]");
    public By chooseYearTo = By.xpath("(//*[@data-value='{\"stringVal\":\"2021\"}'])[2]");
    public By resultRegistrant = By.xpath("//*[@id=\"search-result-container\"]/li/div[1]/a");
    public By resultDates = By.xpath("//div[@class=\"gr-search-registry-date\"]");
}