package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

public class RegistryTest {

    WebDriver driver;
    RegistryPage registryPage;
    WebDriverWait webDriverWait;
    RegistryModel registryModel = new RegistryModel();
    String driverPath = "/driver/chromedriver_mac";

    @BeforeMethod
    public void beforeMethod() {
        System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + driverPath);
        Map<String, Object> prefs = new HashMap<>();
        prefs.put("profile.default_content_setting_values.notifications", 1);

        ChromeOptions options = new ChromeOptions();
        options.setExperimentalOption("prefs", prefs);

        driver = new ChromeDriver(options);
        driver.get("https://www.amazon.com/");
        driver.manage().window().maximize();
        webDriverWait = new WebDriverWait(driver, 15);
    }

    @AfterMethod
    public void afterMethod() {
        driver.quit();
    }

    @Test
    public void successFindRegistrant() {
        registryPage = new RegistryPage(driver);
        registryPage.findRegistrantName(registryModel.registrantName, registryModel.birthdayGiftListValue);
        registryPage.verifyRegistrantAndDate();
    }
}